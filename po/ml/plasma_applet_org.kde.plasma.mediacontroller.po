# Malayalam translations for plasma-workspace package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-16 02:12+0000\n"
"PO-Revision-Date: 2021-02-18 13:23+0000\n"
"Last-Translator: Vivek K J <vivekkj2004@gmail.com>\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 4.4\n"

#: package/contents/ui/AlbumArtStackView.qml:191
#, kde-format
msgid "No title"
msgstr ""

#: package/contents/ui/AlbumArtStackView.qml:191
#: package/contents/ui/main.qml:83
#, kde-format
msgid "No media playing"
msgstr "മീഡിയ പ്ലേ ചെയ്യുന്നില്ല"

#: package/contents/ui/ExpandedRepresentation.qml:407
#: package/contents/ui/ExpandedRepresentation.qml:528
#, kde-format
msgctxt "Remaining time for song e.g -5:42"
msgid "-%1"
msgstr "-%1"

#: package/contents/ui/ExpandedRepresentation.qml:557
#, kde-format
msgid "Shuffle"
msgstr "കലർത്തുക"

#: package/contents/ui/ExpandedRepresentation.qml:583
#: package/contents/ui/main.qml:97
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "മുൻപത്തെ ട്രാക്ക്"

#: package/contents/ui/ExpandedRepresentation.qml:605
#: package/contents/ui/main.qml:105
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "താത്കാലികമായി നിർത്തുക"

#: package/contents/ui/ExpandedRepresentation.qml:605
#: package/contents/ui/main.qml:113
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "പ്ലേ"

#: package/contents/ui/ExpandedRepresentation.qml:623
#: package/contents/ui/main.qml:121
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "അടുത്ത ട്രാക്ക്"

#: package/contents/ui/ExpandedRepresentation.qml:646
#, kde-format
msgid "Repeat Track"
msgstr "ട്രാക്ക് ആവർത്തിക്കുക"

#: package/contents/ui/ExpandedRepresentation.qml:646
#, kde-format
msgid "Repeat"
msgstr "ആവർത്തിക്കുക"

#: package/contents/ui/main.qml:90
#, kde-format
msgctxt "Open player window or bring it to the front if already open"
msgid "Open"
msgstr "തുറക്കുക"

#: package/contents/ui/main.qml:129
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "നിർത്തുക"

#: package/contents/ui/main.qml:142
#, kde-format
msgctxt "Quit player"
msgid "Quit"
msgstr "പുറത്തുകടക്കുക"

#: package/contents/ui/main.qml:250
#, kde-format
msgid "Choose player automatically"
msgstr "പ്ലയർ സ്വയമേവ തിരഞ്ഞെടുക്കുക"

#: package/contents/ui/main.qml:307
#, fuzzy, kde-format
#| msgctxt "by Artist (paused, player name)"
#| msgid "by %1 (paused, %2)"
msgctxt "@info:tooltip %1 is a musical artist and %2 is an app name"
msgid ""
"by %1 (%2)\n"
"Middle-click to pause\n"
"Scroll to adjust volume"
msgstr "%1 ന്റെ (താത്കാലികമായി നിർത്തി, %2)"

#: package/contents/ui/main.qml:307
#, kde-format
msgctxt "@info:tooltip %1 is an app name"
msgid ""
"%1\n"
"Middle-click to pause\n"
"Scroll to adjust volume"
msgstr ""

#: package/contents/ui/main.qml:321
#, fuzzy, kde-format
#| msgctxt "by Artist (paused, player name)"
#| msgid "by %1 (paused, %2)"
msgctxt "@info:tooltip %1 is a musical artist and %2 is an app name"
msgid ""
"by %1 (paused, %2)\n"
"Middle-click to play\n"
"Scroll to adjust volume"
msgstr "%1 ന്റെ (താത്കാലികമായി നിർത്തി, %2)"

#: package/contents/ui/main.qml:321
#, fuzzy, kde-format
#| msgctxt "by Artist (paused, player name)"
#| msgid "by %1 (paused, %2)"
msgctxt "@info:tooltip %1 is an app name"
msgid ""
"Paused (%1)\n"
"Middle-click to play\n"
"Scroll to adjust volume"
msgstr "%1 ന്റെ (താത്കാലികമായി നിർത്തി, %2)"

#~ msgctxt "by Artist (player name)"
#~ msgid "by %1 (%2)"
#~ msgstr "%1 (%2) വക"

#~ msgctxt "Paused (player name)"
#~ msgid "Paused (%1)"
#~ msgstr "താത്കാലികമായി നിർത്തി (%1)"
