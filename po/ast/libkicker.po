# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-31 03:09+0000\n"
"PO-Revision-Date: 2023-05-09 21:46+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#: plugin/actionlist.cpp:86
#, kde-format
msgid "Open with:"
msgstr ""

#: plugin/actionlist.cpp:99
#, kde-format
msgid "Properties"
msgstr "Propiedaes"

#: plugin/actionlist.cpp:146
#, kde-format
msgid "Add to Desktop"
msgstr ""

#: plugin/actionlist.cpp:151
#, kde-format
msgid "Add to Panel (Widget)"
msgstr ""

#: plugin/actionlist.cpp:156
#, kde-format
msgid "Pin to Task Manager"
msgstr ""

#: plugin/actionlist.cpp:307 plugin/rootmodel.cpp:399
#, kde-format
msgid "Recent Files"
msgstr "Ficheros de recién"

#: plugin/actionlist.cpp:317
#, kde-format
msgid "Forget Recent Files"
msgstr ""

#: plugin/actionlist.cpp:395
#, kde-format
msgid "Edit Application…"
msgstr "Editar l'aplicación…"

#: plugin/actionlist.cpp:429
#, kde-format
msgctxt "@action opens a software center with the application"
msgid "Uninstall or Manage Add-Ons…"
msgstr ""

#: plugin/appentry.cpp:308
#, kde-format
msgid "Hide Application"
msgstr ""

#: plugin/appentry.cpp:367
#, kde-format
msgctxt "App name (Generic name)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: plugin/appentry.cpp:369
#, kde-format
msgctxt "Generic name (App name)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: plugin/appsmodel.cpp:36 plugin/appsmodel.cpp:61 plugin/computermodel.cpp:98
#: plugin/computermodel.cpp:226 plugin/recentusagemodel.cpp:153
#: plugin/recentusagemodel.cpp:216
#, kde-format
msgid "Applications"
msgstr "Aplicaciones"

#: plugin/appsmodel.cpp:154
#, kde-format
msgid "Unhide Applications in this Submenu"
msgstr ""

#: plugin/appsmodel.cpp:163
#, kde-format
msgid "Unhide Applications in '%1'"
msgstr ""

#: plugin/computermodel.cpp:92
#, kde-format
msgid "Show KRunner"
msgstr ""

#: plugin/computermodel.cpp:96
#, kde-format
msgid "Search, calculate, or run a command"
msgstr ""

#: plugin/computermodel.cpp:149
#, kde-format
msgid "Computer"
msgstr "Ordenador"

#. i18n.
#: plugin/kastatsfavoritesmodel.cpp:544 plugin/rootmodel.cpp:392
#: plugin/simplefavoritesmodel.cpp:31
#, kde-format
msgid "Favorites"
msgstr ""

#: plugin/recentusagemodel.cpp:151
#, kde-format
msgid "Recently Used"
msgstr ""

#: plugin/recentusagemodel.cpp:156 plugin/recentusagemodel.cpp:298
#, kde-format
msgid "Files"
msgstr "Ficheros"

#: plugin/recentusagemodel.cpp:238
#, kde-format
msgid "Forget Application"
msgstr ""

#: plugin/recentusagemodel.cpp:332
#, kde-format
msgid "Open Containing Folder"
msgstr ""

#: plugin/recentusagemodel.cpp:335
#, kde-format
msgid "Forget File"
msgstr ""

#: plugin/recentusagemodel.cpp:472
#, kde-format
msgid "Forget All"
msgstr ""

#: plugin/recentusagemodel.cpp:474
#, kde-format
msgid "Forget All Applications"
msgstr ""

#: plugin/recentusagemodel.cpp:477
#, kde-format
msgid "Forget All Files"
msgstr ""

#: plugin/rootmodel.cpp:92
#, kde-format
msgid "Hide %1"
msgstr ""

#: plugin/rootmodel.cpp:374
#, kde-format
msgid "All Applications"
msgstr "Toles aplicaciones"

#: plugin/rootmodel.cpp:386
#, kde-format
msgid "This shouldn't be visible! Use KICKER_FAVORITES_MODEL"
msgstr ""

#: plugin/rootmodel.cpp:399
#, kde-format
msgid "Often Used Files"
msgstr ""

#: plugin/rootmodel.cpp:408
#, kde-format
msgid "Recent Applications"
msgstr "Aplicaciones de recién"

#: plugin/rootmodel.cpp:408
#, kde-format
msgid "Often Used Applications"
msgstr ""

#: plugin/rootmodel.cpp:423
#, kde-format
msgid "Power / Session"
msgstr ""

#: plugin/runnermodel.cpp:222
#, kde-format
msgid "Search results"
msgstr ""

#: plugin/systementry.cpp:195
#, kde-format
msgid "Lock"
msgstr ""

#: plugin/systementry.cpp:198
#, kde-format
msgid "Log Out"
msgstr ""

#: plugin/systementry.cpp:201 plugin/systementry.cpp:269
#, kde-format
msgid "Save Session"
msgstr ""

#: plugin/systementry.cpp:204
#, kde-format
msgid "Switch User"
msgstr ""

#: plugin/systementry.cpp:207
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Dormir"

#: plugin/systementry.cpp:210
#, kde-format
msgid "Hibernate"
msgstr ""

#: plugin/systementry.cpp:213
#, kde-format
msgid "Restart"
msgstr "Reaniciar"

#: plugin/systementry.cpp:216
#, kde-format
msgid "Shut Down"
msgstr "Apagar"

#: plugin/systementry.cpp:229 plugin/systementry.cpp:232
#: plugin/systementry.cpp:235 plugin/systementry.cpp:238
#, kde-format
msgid "Session"
msgstr "Sesión"

#: plugin/systementry.cpp:241 plugin/systementry.cpp:244
#: plugin/systementry.cpp:247 plugin/systementry.cpp:250
#, kde-format
msgid "System"
msgstr "Sistema"

#: plugin/systementry.cpp:263
#, kde-format
msgid "Lock screen"
msgstr ""

#: plugin/systementry.cpp:266
#, kde-format
msgid "End session"
msgstr ""

#: plugin/systementry.cpp:272
#, kde-format
msgid "Start a parallel session as a different user"
msgstr ""

#: plugin/systementry.cpp:275
#, kde-format
msgid "Suspend to RAM"
msgstr ""

#: plugin/systementry.cpp:278
#, kde-format
msgid "Suspend to disk"
msgstr ""

#: plugin/systementry.cpp:281
#, kde-format
msgid "Restart computer"
msgstr ""

#: plugin/systementry.cpp:284
#, kde-format
msgid "Turn off computer"
msgstr ""

#: plugin/systemmodel.cpp:31
#, kde-format
msgid "System actions"
msgstr "Aiciones del sistema"
