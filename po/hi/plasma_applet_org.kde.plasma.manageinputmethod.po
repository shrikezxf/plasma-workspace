# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Raghavendra Kamath <raghu@raghukamath.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-11 02:03+0000\n"
"PO-Revision-Date: 2021-07-26 12:49+0530\n"
"Last-Translator: Raghavendra Kamath <raghu@raghukamath.com>\n"
"Language-Team: kde-hindi\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 21.04.3\n"

#: contents/ui/manage-inputmethod.qml:62
#, kde-format
msgctxt "Opens the system settings module"
msgid "Configure Virtual Keyboards..."
msgstr "आभासी कुंजीपटल विन्यस्त करें"

#: contents/ui/manage-inputmethod.qml:81
#, kde-format
msgid "Virtual Keyboard: unavailable"
msgstr "आभासी कुंजीपटल : अनुपलब्ध"

#: contents/ui/manage-inputmethod.qml:95
#, kde-format
msgid "Virtual Keyboard: disabled"
msgstr "आभासी कुंजीपटल : अक्षम"

#: contents/ui/manage-inputmethod.qml:112
#, fuzzy, kde-format
#| msgctxt "Opens the system settings module"
#| msgid "Configure Virtual Keyboards..."
msgid "Show Virtual Keyboard"
msgstr "आभासी कुंजीपटल विन्यस्त करें"

#: contents/ui/manage-inputmethod.qml:128
#, kde-format
msgid "Virtual Keyboard: visible"
msgstr "आभासी कुंजीपटल : दृश्यमान"

#: contents/ui/manage-inputmethod.qml:143
#, kde-format
msgid "Virtual Keyboard: enabled"
msgstr "आभासी कुंजीपटल : सक्षम"
